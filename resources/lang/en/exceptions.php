<?php

return [
	'401' => 'Not authentication.',
	'403' => 'the user does not have authorization to perform the action',
	'404' => [
		'model'=> 'Not found model instance (:model) for id.',
		'url'  => 'Resource not found.'
	],
	'405' => 'Method not allowed for route.',
	'query' => 'It is not possible to delete the resource that is related to another',
	'500' => 'Unexpected error on Server'
];