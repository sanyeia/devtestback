<?php

use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->float('cost',20,2);
            $table->text('description');
            $table->timestamps();
        });

        Product::insert([
            [
                'id' => Uuid::uuid4()->toString(),
                'name' => 'Televisor',
                'cost' => 1000000,
                'description' => 'Televisor de 42" alta definicion y buen tamaño',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => Uuid::uuid4()->toString(),
                'name' => 'Cafetera',
                'cost' => 300000,
                'description' => 'Para siempre tener el mejor cafe en las mañanas',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => Uuid::uuid4()->toString(),
                'name' => 'Licuadora',
                'cost' => 100000,
                'description' => 'Licuadora perfecta para crear cualquier tipo de bebida',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
