<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Auth Routes
Route::post('register', 'Auth\AuthController@store');
Route::post('login', 'Auth\AuthController@login')->name('login');
Route::get('logout', 'Auth\AuthController@logout')->middleware('auth:api');


Route::middleware('auth:api')->group( function () {
    Route::resource('customers', 'Customers\CustomerController')->except(['create', 'edit']);

    Route::post('customer/{customer}/addresses', 'Customers\AddressController@store');
    Route::resource('addresses', 'Customers\AddressController')->only(['update', 'destroy']);

    Route::post('addresses/{address}/invoices', 'Invoice\InvoiceController@store');
    Route::resource('invoices', 'Invoice\InvoiceController')->except(['create', 'store', 'edit']);

    Route::resource('products', 'Products\ProductController')->except(['create', 'edit', 'show']);
});
