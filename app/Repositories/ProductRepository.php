<?php

namespace Repositories;

use App\Exceptions\ConflictException;
use App\Models\Product;

class ProductRepository extends BaseRepository
{
    /**
     * Devuelve un querybuilder del recurso solicitado
     *
     * @return Builder
     */
    public static function index(){
        return Product::orderBy('name','desc');
    }

    /**
     * Crea una nueva instancia del recurso solicitado
     *
     * @return Product
     */
    public static function create(array $data){
        //se comprueba que la informacion enviada sea valida
        parent::validateData($data, [
            'name' => 'required',
            'cost' => 'required|numeric|min:0',
            'description' => 'sometimes',
        ]);

        //se crea el Product
        $result = new Product();
        $result->fill($data);
        $result->save();

        return $result;
    }

    /**
     * Actualiza una instancia del recurso solicitado
     *
     * @param Product $instance
     * @param array $data
     * @return Product
     */
    public static function update(Product $instance, array $data){
        if(empty($data)){ return $instance;}

        //se comprueba que la informacion enviada sea valida
        parent::validateData($data, [
            'name' => 'sometimes|required',
            'cost' => 'sometimes|required|numeric|min:0',
            'description' => 'sometimes',
        ]);

        //se actualiza el Product
        $instance->fill($data);
        $instance->save();

        return $instance;
    }

    /**
     * Borra una instancia del recurso solicitado
     *
     * @param Product $instance
     * @return Product
     */
    public static function delete(Product $instance){
        //se comprueba que no existan ventas asociadas a esta direccion
        if($instance->invoices()->count()){
            throw new ConflictException('No se puede borrar este producto ya que esta asociado a una factura');
        }

        //se borra el Product
        $instance->delete();
        return $instance;
    }
}
