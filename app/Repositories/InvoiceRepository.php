<?php

namespace Repositories;

use App\Models\Address;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\User;
use Ramsey\Uuid\Uuid;

class InvoiceRepository extends BaseRepository
{
    /**
     * Devuelve un querybuilder del recurso solicitado
     *
     * @return Builder
     */
    public static function index(){
        return Invoice::with('customer')->orderBy('invoices.created_at','desc');
    }

    /**
     * Crea una nueva instancia del recurso solicitado
     *
     * @return Invoice
     */
    public static function create(Address $address, array $data, array $products, User $created_by = null){
        //se comprueba la informacion de la factura
        parent::validateData($data, [
            'notes' => 'sometimes',
            'status' => 'sometimes|required|in:1,2,3,4,5',
        ]);

        //se comprueba que los productos sean validos
        parent::validateData($products, [
            '*.product_id' => 'required|string|exists:App\Models\Product,id',
            '*.amount' => 'required|integer|min:1',
        ]);

        $prodlist = [];
        $total = 0;
        foreach ($products as $value) {
            //se va creando en array de array que necesita el metodo de attach de eloquent
            $prodlist[ $value['product_id'] ] = [
                'amount' => $value['amount'],
                'id' => Uuid::uuid4()->toString(),
                'created_at' => now(),
                'updated_at' => now()
            ];

            /**
             * se busca el producto para obtener su valor
             * NOTA: se puede hacer tambien afuera del foreach con un whereIn para evitar N cantidad de consultas pero debido a
             * que es una factura en la que en teoria no deberian haber muchos productos esta fue la forma que escogí
             */
            $prod = Product::find($value['product_id']);
            $total += $prod->cost*$value['amount'];
        }

        //se crea el Invoice
        $result = new Invoice();
        $result->user_id = isset($created_by) ? $created_by->id : null;
        $result->customer_id = $address->customer_id;
        $result->address_id = $address->id;
        $result->total = $total;
        $result->fill($data);
        $result->save();

        //se añaden los productos de la factura
        $result->products()->attach($prodlist);

        $result->products;
        $result->customer;
        return $result;
    }

    /**
     * Actualiza una instancia del recurso solicitado
     *
     * @param Invoice $instance
     * @param array $data
     * @return Invoice
     */
    public static function update(Invoice $instance, array $data){
        if(empty($data)){ return $instance;}

        //se comprueba la informacion de la factura
        parent::validateData($data, [
            'notes' => 'sometimes',
            'status' => 'sometimes|required|in:1,2,3,4,5',
        ]);

        //se actualiza el Invoice
        $instance->fill($data);
        $instance->save();

        return $instance;
    }

    /**
     * Borra una instancia del recurso solicitado
     *
     * @param Invoice $instance
     * @return Invoice
     */
    public static function delete(Invoice $instance){
        //se borra el Invoice
        $instance->delete();
        return $instance;
    }
}
