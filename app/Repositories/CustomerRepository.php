<?php

namespace Repositories;

use App\Exceptions\ConflictException;
use App\Exceptions\GeneralException;
use App\Exceptions\ValidationFailExeption;
use App\Models\Address;
use App\Models\Invoice;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

class CustomerRepository extends BaseRepository
{
    /**
     * Devuelve un querybuilder del recurso solicitado
     *
     * @return Builder
     */
    public static function index(){
        return Customer::orderBy('created_at', 'desc');
    }

    /**
     * Crea una nueva instancia del recurso solicitado
     *
     * @return Customer
     */
    public static function create(array $data, array $addresses = []){
        //se comprueba que la informacion enviada sea valida
        parent::validateData($data, [
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required|max:25',
            'email' => 'required|email',
        ]);

        DB::beginTransaction();
        try {
            //se crea el customer
            $result = Customer::create($data);

            if( !empty($addresses) ){
                AddressRepository::createMultiple($result, $addresses);
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            if($th instanceof ValidationFailExeption){ throw $th; }
            throw new GeneralException('Error al crear el registro', 500);
        }

        $result->addresses;
        return $result;
    }

    /**
     * Actualiza una instancia del recurso solicitado
     *
     * @param Customer $instance
     * @param array $data
     * @return Customer
     */
    public static function update(Customer $instance, array $data){
        if(empty($data)){ return $instance;}

        //se comprueba que la informacion enviada sea valida
        parent::validateData($data, [
            'firstname' => 'sometimes|required',
            'lastname' => 'sometimes|required',
            'phone' => 'sometimes|required|max:25',
            'email' => 'sometimes|required|email',
        ]);

        //se actualiza el customer
        $instance->fill($data);
        $instance->save();

        return $instance;
    }

    /**
     * Borra una instancia del recurso solicitado
     *
     * @param Customer $instance
     * @return Customer
     */
    public static function delete(Customer $instance){
        //se comprueba que el cliente no tenga direcciones ni ventas
        if( Address::where('customer_id',$instance->id)->exists() or Invoice::where('customer_id',$instance->id)->exists() ){
            throw new ConflictException('No se puede borrar el Cliente ya que tiene informacion asociada');
        }

        //se borra el cliente
        $instance->delete();
        return $instance;
    }
}
