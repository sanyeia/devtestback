<?php

namespace Repositories;

use App\Exceptions\ValidationFailExeption;
use Illuminate\Support\Facades\Validator;

class BaseRepository
{
    public static function validateData($data, $rules){
        $validator = Validator::make($data, $rules);

        //si falla el validador tirar error de validacion
        if($validator->fails()){ throw new ValidationFailExeption($validator); }

        return true;
    }
}
