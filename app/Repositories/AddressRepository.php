<?php

namespace Repositories;

use App\Exceptions\ConflictException;
use App\Models\Address;
use App\Models\Customer;
use Ramsey\Uuid\Uuid;

class AddressRepository extends BaseRepository
{
    /**
     * Crea una nueva instancia del recurso solicitado
     *
     * @return Address
     */
    public static function create(Customer $customer, array $data){
        //se comprueba que la informacion enviada sea valida
        parent::validateData($data, [
            'address' => 'required',
            'description' => 'sometimes',
            'country' => 'sometimes|required|max:25',
            'state' => 'sometimes|required',
            'city' => 'sometimes|required',
        ]);

        //se crea el Address
        $result = new Address();
        $result->customer_id = $customer->id;
        $result->fill($data);
        $result->save();

        return $result;
    }

    /**
     * Crea varios registros del recurso solicitado
     *
     * @return boolean
     */
    public static function createMultiple(Customer $customer, array $data){
        //se comprueba que la informacion enviada sea valida
        parent::validateData($data, [
            '*' => 'required|array',
            '*.address' => 'required',
            '*.description' => 'sometimes',
            '*.country' => 'sometimes|required|max:25',
            '*.state' => 'sometimes|required',
            '*.city' => 'sometimes|required',
        ]);

        $create = [];
        foreach ($data as $value) {
            $create[] = [
                'id' => Uuid::uuid4()->toString(),
                'customer_id' => $customer->id,
                'address' => $value['address'],
                'description' => $value['description'] ?? '',
                'country' => $value['country'] ?? '',
                'state' => $value['state'] ?? '',
                'city' => $value['city'] ?? '',
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        $result = Address::insert($create);

        return $result;
    }

    /**
     * Actualiza una instancia del recurso solicitado
     *
     * @param Address $instance
     * @param array $data
     * @return Address
     */
    public static function update(Address $instance, array $data){
        if(empty($data)){ return $instance;}

        //se comprueba que la informacion enviada sea valida
        parent::validateData($data, [
            'address' => 'sometimes|required',
            'description' => 'sometimes',
            'country' => 'sometimes|required|max:25',
            'state' => 'sometimes|required',
            'city' => 'sometimes|required',
        ]);

        //se actualiza el Address
        $instance->fill($data);
        $instance->save();

        return $instance;
    }

    /**
     * Borra una instancia del recurso solicitado
     *
     * @param Address $instance
     * @return Address
     */
    public static function delete(Address $instance){
        //se comprueba que no existan ventas asociadas a esta direccion
        if($instance->invoices()->count()){
            throw new ConflictException('No se puede borrar esta direccion ya que tiene informacion asociada');
        }

        //se borra el Address
        $instance->delete();
        return $instance;
    }
}
