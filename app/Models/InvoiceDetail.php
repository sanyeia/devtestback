<?php

namespace App\Models;

use App\Traits\UuidTrait;

class InvoiceDetail extends ApiModel
{
    use UuidTrait;

    protected $table = 'invoice_details';
    public $incrementing = false;
}
