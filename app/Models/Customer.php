<?php

namespace App\Models;

use App\Traits\UuidTrait;

class Customer extends ApiModel
{
    use UuidTrait;

    protected $table = 'customers';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'phone',
    ];

    public function addresses(){
        return $this->hasMany('App\Models\Address', 'customer_id');
    }

    public function invoices(){
        return $this->hasMany('App\Models\Invoice', 'customer_id');
    }
}
