<?php

namespace App\Models;

use App\Traits\UuidTrait;

class Invoice extends ApiModel
{
    use UuidTrait;

    protected $table = 'invoices';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notes',
        'status', //1 = En proceso; 2 = Aprobada; 3 = Rechazada; 4 = Enviada; 5 = Cancelada
    ];

    public function products(){
        return $this->belongsToMany('App\Models\Product', 'invoice_details')
        ->as('details')
        ->withPivot('amount');
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
}
