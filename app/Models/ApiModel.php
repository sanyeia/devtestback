<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

abstract class ApiModel extends Model
{

    /** ___
     * | _ \ ___  ___ _ __  ___  _ _   ___ ___  _ _
     * |   // -_)(_-<| '_ \/ _ \| ' \ (_-</ -_)| '_|
     * |_|_\\___|/__/| .__/\___/|_||_|/__/\___||_|
     *               |_|
     * By: Andres Arroyo
     */

    /**
     * Paginacion personalizada
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param boolean $nopaginate = si se debe paginar la respuesta (default: false)
     * @param boolean $filter = si se deben aplciar los filtros a la respuesta (default: true)
     * @param boolean $noData = si se debe devolver dentro de data la respuesta (default: false)
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeShowAll($query, $nopaginate = false)
    {
        //resultados por pagina
        $per_page = (isset(request()->per_page) and is_numeric(request()->per_page)) ? request()->per_page : 15;

        //ordenamientos
        $query = $this->sortData($query);

        //filtros
        $this->filterData($query);

        //paginar o no
        if(request()->has('nopagination') or $nopaginate){
            $result = $query->get();
        }else{
            $result = $query->paginate($per_page);
            //para que salga todo el link con parametros
            $result->appends(request()->input())->links();
        }

        return $result;
    }

    protected function sortData($query)
	{
        //se revisa si se envio alguno de los dos parametros de ordenamiento en la peticion
        $column = null;
        if (request()->has('sort_by')) {
            $column = $this->getColumName($query, request()->sort_by);
            //en el caso de ordenamientos por multiples campos, se agrega un "DESC," en todos los campos
            //ya que si no se pone mysql pone por defecto ASC, por eso en el caso de sort_by se quita el "DESC,"
            $column = preg_replace('/DESC,/', 'ASC,', $column);
			$order = "$column ASC";
		} elseif(request()->has('sort_by_desc')){
            $column = $this->getColumName($query, request()->sort_by_desc);
			$order = "$column DESC";
        }

        if( $column != null ){
            //se le quita cualquier ordenamiento por defecto que tenga la consulta
            $query->getQuery()->orders = null;
            //se le agrega el ordenamiento solicitado
            $query = $query->orderByRaw($order);
        }

		return $query;
    }

	protected function filterData($query)
	{
        foreach (request()->query() as $key => $value) {
            $column = $this->getColumName($query, $key);
            if($column != null){
                $query = $query->where($column, $value);
			}
		}

		return $query;
    }

    /**
     * determina si se puede ordernar por el campo enviado, osea si el campo
     * esta en la consulta
     *
     * @return string|null
     */
    private function getColumName($query, $key) {
        //se revisa si el parametro enviado es una columna de la tabla
        $attribute = $this->isColumns($key) ? "$this->table.$key" : null;

        //obtenemos el select de la consulta en caso que se haya especificado
        $selects = $query->getQuery()->columns;
        if( $attribute == null and $selects != null){
            //recorremos las columnas especificadas en el select
            foreach ($selects as $value) {
                $field = explode('.',$value);
                $table = '';
                //se revisan los campos del formato: "table.column" y "table.column as name"
                if( isset($field[1]) and strpos($field[1], ' as ') ){
                    //se obtiene la tabla a la que pertenece
                    $table = "$field[0].";

                    //se obtiene el nombre de la columna y en caso de ser la buscada se asigna para devolver
                    $field = $field[1];
                    $field = explode(' as ',$field);
                    if($field[1] == $key){
                        $attribute = $table.$field[0];
                    }
                }else{
                    //ahora los que solo se les definió el nombre de la columna: "column"
                    //y en caso de ser la buscada se asigna para devolver
                    if($field[0] == $key){
                        $attribute = $field[0];
                    }
                }
            }
        }

        return $attribute;
    }

    /**
     * verificar si la tabla tiene una columna
     * @return bool
     */
    public function isColumns($key) {
        return Schema::hasColumn($this->table, $key);
    }
}
