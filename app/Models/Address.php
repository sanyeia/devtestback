<?php

namespace App\Models;

use App\Traits\UuidTrait;

class Address extends ApiModel
{
    use UuidTrait;

    protected $table = 'addresses';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address',
        'description',
        'country',
        'state',
        'city',
    ];

    public function invoices(){
        return $this->hasMany('App\Models\Invoice', 'address_id');
    }
}
