<?php

namespace App\Models;

use App\Traits\UuidTrait;

class Product extends ApiModel
{
    use UuidTrait;

    protected $table = 'products';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'cost', 'description'];

    public function invoices(){
        return $this->belongsToMany('App\Models\Product', 'invoice_details')->withPivot('amount');
    }
}
