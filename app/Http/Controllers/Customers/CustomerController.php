<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Repositories\CustomerRepository;

class CustomerController extends Controller
{
    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = CustomerRepository::index()->showAll();
        return $this->successResponse($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'customer' => 'required|array',
            'addresses' => 'sometimes|array',
            'addresses.*' => 'sometimes|required|array',
        ]);

        $addr = $request->addresses ?? [];
        $result = CustomerRepository::create($request->customer, $addr);
        return $this->successResponse($result, 'Cliente creado con exito', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        $customer->addresses;
        return $this->successResponse($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $this->validate($request, [
            'customer' => 'required|array',
        ]);

        $result = CustomerRepository::update($customer, $request->customer);
        return $this->successResponse($result, 'Cliente actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $result = CustomerRepository::delete($customer);
        return $this->successResponse($result, 'Cliente borrado exitosamente');
    }
}
