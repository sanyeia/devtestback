<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Customer;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Repositories\InvoiceRepository;

class InvoiceController extends Controller
{
    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = InvoiceRepository::index()->showAll();
        return $this->successResponse($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Address $address)
    {
        $this->validate($request, [
            'invoice' => 'required|array',
            'products' => 'required|array',
        ]);

        $result = InvoiceRepository::create($address, $request->invoice, $request->products, $request->user());
        return $this->successResponse($result, 'Factura creada con exito', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        $invoice->products;
        $invoice->load('customer.addresses');
        return $this->successResponse($invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $this->validate($request, [
            'invoice' => 'required|array',
        ]);

        $result = InvoiceRepository::update($invoice, $request->invoice);
        return $this->successResponse($result, 'Factura actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        $result = InvoiceRepository::delete($invoice);
        return $this->successResponse($result, 'Factura borrada exitosamente');
    }
}
