<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Registrar nuevo usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user' => 'required|array',
            'user.firstname' => 'required',
            'user.lastname' => 'required',
            'user.email' => 'required|email',
            'user.password' => 'required',
        ]);

        if( User::where('email', $request->user['email'])->exists() ){
            return $this->errorResponse('Usuario ya existe en el sistema, por favor Inicia sesion', 409);
        }

        //se crea el usuario
        $user = User::create($request->user);

        //se le crea el token para que pueda acceder automaticamente
        $success['token'] =  $user->createToken('NewUToken')->accessToken;
        $success['user'] =  $user;

        return $this->successResponse($success, 'Usuario creado con exito');
    }

    /**
     * Login de un usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        //en caso de que las credenciales no sean validas
        if( !Auth::attempt(['email' => $request->email, 'password' => $request->password]) ){
            return $this->errorResponse('Email y/o contraseña incorrecta', 401);
        }

        //se obtiene el usuario y se le crea el token
        $user = Auth::user();
        $success['token'] =  $user->createToken('NormalUToken')->accessToken;
        $success['user'] =  $user;

        return $this->successResponse($success);
    }

    /**
     * Logout de un usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $user = Auth::user();
        $accessToken = $user->token();
        $accessToken->revoked = true;
        $accessToken->save();

        DB::table('oauth_refresh_tokens')->where('access_token_id', $accessToken->id)->update(['revoked' => true]);

        $success['user'] =  $user;

        return $this->successResponse($success);
    }
}
