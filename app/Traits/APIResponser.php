<?php

namespace App\Traits;

trait APIResponser {

    /**
     * formatear las respuetas que devuelva el api para que se mantenga una estructura
     *
     * @param $data
     * @param string $message = 'Ok'
     * @param integer $code = 200
     * @return void
     */
    protected function successResponse($data, string $message = 'Ok', $code = 200)
	{
        if($data instanceof \Illuminate\Pagination\LengthAwarePaginator){
            $result = collect([
                'message' => $message,
                'code' => $code,
            ]);
            $result = $result->merge($data);
        }else{
            $result = [
                'data' => $data,
                'message' => $message,
                'code' => $code,
            ];
        }

		return response()->json($result, $code);
	}

    /**
     * formatear las respuestas de error para mantener una estructura
     *
     * @param $message
     * @param integer $code
     * @return void
     */
	protected function errorResponse($message, $code = 400)
	{
        $result = [
            'message' => $message,
			'code' => $code,
        ];

		return response()->json($result, $code);
    }
}
