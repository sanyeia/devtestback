<?php

namespace App\Traits;

use Ramsey\Uuid\Uuid;

trait UuidTrait {

    /**
     * sobrescribe el el metodo de save de los modelos para agregar la validacion
     * que en en el caso que no este seteado el id en el modelo que se intenta guardar
     * se genere un Uuid
     *
     * @param array $options
     * @return void
     */
	public function save(array $options = []){
		if(!isset($this->id)){
			$this->id = Uuid::uuid4()->toString();
		}
		parent::save($options);
	}
}

