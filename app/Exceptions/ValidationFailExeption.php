<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Exception;
use Illuminate\Validation\Validator;

class ValidationFailExeption extends Exception
{
    use ApiResponser;

    private $validator;

    public function __construct(Validator $validator) {
        parent::__construct();
        $this->validator = $validator;
    }

    public function render($request)
    {
        $errors = $this->validator->errors()->getMessages();
        return $this->errorResponse($errors, 422);
    }
}
