<?php

namespace App\Exceptions;

use App\Traits\APIResponser;
use Exception;

class ConflictException extends Exception
{
    use APIResponser;

    public $message;

    public function __construct(string $message = 'Invalid Request - Conflict') {
        parent::__construct();
        $this->message = $message;
    }

    public function render($request)
    {
        return $this->errorResponse($this->message, 409);
    }
}
