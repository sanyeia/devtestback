<?php

namespace App\Exceptions;

use App\Traits\APIResponser;
use Exception;

class GeneralException extends Exception
{
    use APIResponser;

    public $message;
    public $code;

    public function __construct(string $message = 'Invalid Request', $code = 400) {
        parent::__construct();
        $this->message = $message;
        $this->code = $code;
    }

    public function render($request)
    {
        return $this->errorResponse($this->message, $this->code);
    }
}
