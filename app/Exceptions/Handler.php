<?php

namespace App\Exceptions;

use App\Traits\APIResponser;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Throwable;

class Handler extends ExceptionHandler
{
    use APIResponser;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if($exception instanceof ValidationException) {
            $errors = $exception->validator->errors()->getMessages();
            return $this->errorResponse($errors, 422);
        }
        if($exception instanceof ModelNotFoundException) {
            $model = class_basename($exception->getModel());
            $msg = __('exceptions.404.model', ['model'=>$model]);
            return $this->errorResponse($msg, 404);
        }
        if($exception instanceof AuthenticationException) {
            return $this->errorResponse(__('exceptions.401'), 401);
        }
        if($exception instanceof AuthorizationException) {
            return $this->errorResponse(__('exceptions.403'), 403);
        }
        if($exception instanceof NotFoundHttpException) {
            return $this->errorResponse(__('exceptions.404.url'), 404);
        }
        if($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse(__('exceptions.405'), 405);
        }
        if($exception instanceof FileNotFoundException) {
            return $this->errorResponse('File not found', 404);
        }
        if($exception instanceof HttpException and $exception->getStatuscode() == 413) {
            return $this->errorResponse('File size too large', 413);
        }
        if($exception instanceof HttpException) {
            $msg  = $exception->getMessage();
            $code = $exception->getStatuscode();
            return $this->errorResponse($msg, $code);
        }

        if($exception instanceof QueryException) {
            $code = $exception->getCode();
            if($code == 1451){
                //cuando se intenta borrar un registro padre
                return $this->errorResponse(_('exceptions.foreign_fail.delete_update'), 409);
            } elseif($code == 1062 ){
                //cuando un registro esta duplicado
                return $this->errorResponse(_('exceptions.duplicate_entry'), 409);
            } elseif($code == 1452 ) {
                //cuando se intenta actualizar o crear un registro y la FK falla
                return $this->errorResponse(_('exceptions.foreign_fail.add_update'), 409);
            } elseif($code == 1048 ) {
                //cuando se crea un registro y hay un campo que no se ingreso y no tiene valor por defecto
                return $this->errorResponse(_('exceptions.column_cannot_be_null'), 409);
            } elseif($code == 2002 ) {
                //en caso que no se pueda conectar a la base de datos
                return $this->errorResponse( 'Server Unavailable', 503);
            }
        }
        return parent::render($request, $exception);
    }

}
